package klassika;
public class Arvutus {
    public String muutujaA="";
    public String muutujaB="";
    public String kuva;
    public double vastus;
    public String tehe="";
    public String[] tehted = new String[]{"*", "/", "+", "-"};
    
    /**
     * Kui nuppu vajutati, siis edastatakse number vajutusNumber meetodile
     */
    public void vajutus(String nupp){
        for(int i=0; i<10;i++){
            if(nupp.equals(i+"")){
                vajutusNumber(nupp);
            }
        }

        /**
         * Tehte nuppu vajutades muudetakse tehte nupp ära
         */
        for(int i=0; i<tehted.length;i++){
            if(nupp.equals(tehted[i])) {
                if(!muutujaA.isEmpty()){
                    this.tehe = nupp;
                    kuva = muutujaA + " " + tehe;
                }
            }
        }
        
        /**
         * If laused, mis kutsuvad vastava nupu vajutamisel vajalike meetodeid.
         */
        if(nupp.equals("C")){
            tyhjenda();
            kuva = "";
        }

        if(nupp.equals(".")){
            vajutusNumber(nupp);
        }

        if(nupp.equals("=")){
            arvuta();
        }

        if(nupp.equals("+-")){
            m2rgimuutus();
        }

        if(nupp.equals("del")){
            kustuta();
        }

    }
    /**
     * Meetod, mis otsustab, kas hetkel on sisestamisel muutuja A või B (Kui tyhi v2li siis A),
     * kui see on kindlaks tehtud lisab muutujale vastava Stringi(number mida vajutati)
     */
    public void vajutusNumber(String number){
        if(tehe.isEmpty()){
            //muutujaA
            muutujaA = muutujaA.concat(number);
            kuva = muutujaA;
        }else{
            //muutujaB
            muutujaB = muutujaB.concat(number);
            kuva = muutujaA +" "+tehe+" "+muutujaB;
        }
    }
    /**
     * Meetod, mis muudab muutujad ja tehtem2rgi tyhjaks Stringiks
     */
    public void tyhjenda(){
        muutujaA = "";
        muutujaB = "";
        tehe = "";
    }
    /**
     * Meetod, mis otsustab, kas hetkel on sisestamisel muutuja A või B (Kui tyhi v2li siis A),
     * kui see on kindlakse tehtud, muudab m2rki
     */
    public void m2rgimuutus(){
        
        if(tehe.isEmpty()){
            //muutujaA
            double dblA = Double.parseDouble(muutujaA) * -1;
            muutujaA = dblA + "";
            kuva = muutujaA;
        }else{
            //muutujaB
            double dblB = Double.parseDouble(muutujaB) * -1;
            muutujaA = dblB+"";
            kuva = muutujaA +" "+tehe+" "+muutujaB;
        }
    }
    
    /**
     * Esmalt otsustame kas tegeleme muutuja A või B
     * Enne kustutamist kontrollime, kas muutuja on pikem kui 1 tähte
     */
    public void kustuta(){
        if(tehe.isEmpty()){
            //muutujaA
            muutujaA = muutujaA.substring(0, muutujaA.length()-1);
            kuva = muutujaA;
        }else{
            //muutujaB
            muutujaB = muutujaB.substring(0, muutujaB.length()-1);
            kuva = muutujaA +" "+tehe+" "+muutujaB;
        }
    }
    /**
     * Meetod, mis sooritab kalkulatsioonid
     */
    public void arvuta(){
        if(!muutujaA.isEmpty() && !muutujaB.isEmpty()) {
            double dblA = Double.parseDouble(muutujaA);
            double dblB = Double.parseDouble(muutujaB);
            switch (tehe) {
                case "+":
                    vastus = dblA + dblB;
                    break;
                case "-":
                    vastus = dblA - dblB;
                    break;
                case "/":
                    vastus = dblA / dblB;
                    break;
                case "*":
                    vastus = dblA * dblB;
                    break;
            }
            kuva = "" + vastus;
            tyhjenda();
            muutujaA = "" + vastus;
        }
    }

    public String getKuva() {
        return kuva;
    }
}
