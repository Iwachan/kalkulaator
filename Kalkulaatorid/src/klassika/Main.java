package klassika;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        Arvutus kalk = new Arvutus();

        VBox vbox = new VBox();
        Label kuva = new Label("");
        
        kuva.setMinSize(420, 40);
        kuva.setFont(Font.font(30));
        kuva.setAlignment(Pos.CENTER_RIGHT);

        GridPane grid = new GridPane();
        vbox.getChildren().addAll(kuva, grid);

        Scene scene = new Scene(vbox, 416, 376);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Kalkulaator");
        primaryStage.setResizable(false);
       
        /**
         * Loon Masiivi, kus on nuppudele vajalikud t2hised
         */
        String[][] nupud = new String[][]{{"1", "2", "3", "del", "C"},
                                            {"4", "5", "6", "*", "/"},
                                            {"7", "8", "9", "+", "-"},
                                            {"0", ".", "+-", "=", " "}};
        /**
         * Joonistan for loopi kasutades vajalikud nupud ekraanile                                    	
         */
        for(int i=0;i<nupud.length;i++){
            for(int j=0;j<nupud[i].length;j++){
                Button btn = new Button(nupud[i][j]);

                btn.setOnAction(event -> {
                    kalk.vajutus(btn.getText());
                    kuva.setText(kalk.getKuva());
                });

                grid.add(btn, j, i+1);
                btn.setMinSize(85, 85);
                
            }
        }
        
        primaryStage.show();

    }
}
